package qr;

/**
	@author 作者： CGB.NUTONY 
	@author 邮箱：chenzs@tedu.cn
	@version 版本：1.0 上午10:31:52
	@desc 功能：生成自己的二维码
*/

public class TestQR {
	public static void main(String[] args) {
		//网址必须写上协议头http://
		String url = "http://act.codeboy.com/";
		CreateQR.make(150, 150, url, "d:/qr.png");
	}
}
