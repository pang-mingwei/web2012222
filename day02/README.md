# 知识回顾
### git 团队协作代码管理工具
gitee码云 组建团队，只能5人，超过付费
初级程序员要求：会使用git客户端即可，git.exe

### 日常每天处理命令：
1）从git仓库中pull 拉取数据，clone是第一次拉取（一上班）  
2）提交代码（写完的代码才上传） add、commit、push（如果某个功能写完全，测试无误）  
git add README.md 把这个文件新增到本地索引（暂存区）  
git add .  批量就是.，代表把当前工作空间的变化内容（新增、修改、删除）一次提交  

commit 把暂存区执行日志，进行落地，把它们的变化真实提交到本地仓库  
git commit -m "更新提交"  

push 把本地仓库内容同步到远程仓库，两边数据一致    
git push -u origin master 提交到master主分支    
git push   

### 架构图
体系程序执行结构，体系所用技术
![](README_files/1.jpg)

### 前端技术
![](README_files/2.jpg)

html升级h5，它干掉flash，削弱ios和andriod app  
css升级css3，升级bootstrap，升级element-ui 饿了么  
javascript升级jQuery，升级Vue 阿里  

### 1）margin和padding区别？    
![](README_files/3.jpg)  

盒子模型，  
padding 是元素内部空开  
margin 是元素外部空开  

### 2）电影票  
```
<td colspan="2">横向合并单元格</td>
<td rowspan="2">纵向合并单元格</td>
<style>
	td{
		border-radius: 5px; //css设置圆角
	}
</style>
```

3）谷歌二维码？    
二维码本质，图片背后其实一串字符，对应网址，  
扫一扫后会自动用手机浏览器打开这个网址
http://www.baidu.com  
http://act.codeboy.com  

开发步骤：  
1）导入谷歌的第三方jar包，你要去了解api才能开发  
2）老师给封装，CreateQR.make(width，height，url，图片的名称）  
3）图片拷贝到项目中就可以使用  


## 总结：
### 生成自己二维码
google提供java的api，zxing，qr.jar 实现更简洁实用    
CreateQR.make(宽度、高度、网址、生成图片路径)  
利用api把文字形成二维码图片，手机扫描  

开发步骤：  
1）下载第三方 qr.jar  
2）在java工程中创建lib目录，把jar放入  
3）qr/TestQR，同包无需import导包，java可以识别；如果包路径不同，必须import  
4）磁盘生成图片考入Hbuilder，供页面使用images/qr.png  

### 把html升级h5标准，css升级css3
新标签 <audio>、<video>。。。  
css3动画 transition拉伸、animation动画、transform变形。。。

### h5+css3把flash干掉，网站小游戏
削弱ios和andriod，移动app，有一个致命问题，软件公司要为同一个app写两套程序  
所有app都需要写两套。工作量加倍，人员配置加倍。  
软件项目组：写ios一堆人，写andriod一堆人，成本很高。  
随着h5+css3，本质网页，功能强大，浏览器兼容，谷歌可以跑通，IE播放不了。  
iso和andriod都支持浏览器，h5通用，替代iso和andriod，98%功能实现  
只需写一套程序，开发效率高，人员减小一半  ，

### 基本特效
border-radius：值超过图片半径，正方形图片  
使用ul和li标签，默认很难看，使用css进行修饰  
display: inline-table 效果看，它撑满这个区域，类似<td>方式  

### 动态特效
动画 transition、animation（了解）

### 播放音频和视频
以前浏览器插件，flash插件，单独配置；  
浏览器直接支持H5标签  

### 路径
绝对路径：d:/qr.png，把代码复制到你的机器上，失效，需要根据自己的情况设置  
相对路径：images/qr.png，把代码复制到你的机器上，照样使用




