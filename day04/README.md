# 知识回顾
### javascript
h5、css3、js成为网页开发必会技术  
对于后端工程师而言，了解基础，照猫画虎足以  
在实际开发中，不是每个都背写出来，百度，demo，改成自己  

h5+css3只能实现静态网站，网页一旦写好，它内容是不能变化。  
h5+css3+javascript它就可以实现动态网站  
体现：    
1）写代码来设置页面元素值（利用js修改a标签名字和href链接）    
2）javascript和后端技术对接（node、java springmvc)  

### javascript、js、es6、typescript  
1）js是javascript简称  
2）es6是标准化组织ECMAScript，维护js标准化，所有浏览器都遵守这个标准  
3）js衍生ajax，它负责把页面请求发送给后端java  
5）js衍生json，替代xml（极大的萎缩）  
6）js衍生typescript，js只能局限浏览器，typescript+nodejs服务  

### bootstrap
css国际标准，css3版本升级，bootstrap第三方推特发明  
国外市场瞩目，它发明栅格体系，一套代码可以桌面PC端，移动端自适应  
漂亮界面，css3开发者自己写，软件团队自己（美工）  
可以适应主流浏览器，不会换个浏览器界面乱掉（浏览器兼容性）  
bootstrap在国外非常流行，借鉴它发展国内element-ui 饿了吗  

### css和bootstrap怎么学习呢？区别？
学css，要去死记这些常见值：color/font-size/line-height  
bootstrap记忆规范，根据demo去学习，根据实现效果去学习  
重点就学习bootstrap定义好的这些样式类form-group  

### 学习js和h5和css3有什么不同？
js比它们复杂很多，类似java语言，学语法，学api  
document.getElementById("name") 获取到页面对象  

### javascript 的DOM树？
Document Object Model 代表html页面（最多）  
Browser Object Model 代表浏览器对象（很少）window.alert()  

为什么要诞生dom树呢？  
有html，找一个元素，姓名输入框，按顺序解析（查询慢）  
数据结构，树形结构检索效率高。可能3级就可以找到。  
最关心表单元素，  

如何构建dom树呢？  
根据扫描（从上到下遍历html文件）形成dom，  
按元素类型  
无法预估页面某个元素类型一样个数，干脆都是数组Elements  
如果页面只有一个，数组的个数1  

dom树构建好了，怎么访问呢？  
js出品了一套访问标准，api  
1）tagName		document.getElementsByTagName  
2）className		document.getElementsByClassName  
3）name			document.getElementsByName  
4）id					document.getElementById  
js规定id在页面上表达唯一值，  
radio最终提交要么男要么女  
checkbox把提交内容作为一个数组提交：["乒乓球","爬山"]  
开发者要自己控制页面的id唯一要求！！！  

### es6新标准
在js中万物var，对象Object，和java一样，它也是面向对象语言  
es6是js高版本，block scoping 块域，变量更加作用域小，释放越快，代码精度越高  
let替代var，const常量（不允许修改）  
箭头函数 arrow function写法不同，this会有新的定义，不能乱用  
箭头函数只是简化js函数，不能完全实现所有函数写法，（坑）  

java是静态语言，特点在编译时就会对代码进行检查，出错，不能继续  
在运行前就能保障程序的安全性。黑客改不了  
javascript是动态语言，边解析边执行，动态给对象添加属性和方法  
这点java做不到。这里优点程序灵活，缺点安全性。黑客改动代码  

### this在普通函数和箭头函数中代表不同对象
this 在普通函数中代表当前对象  
this 在箭头函数中代表window对象，不能使用！！！！  

### 今日 jQuery，js升级版本(第三方公司出品)
大家先现在这个js  
去 nutony/res下载js/目录下的所有的文件，jquery.min.js  

### jquery
jquery在js基础上进行封装，改变js日常写法，提供新机制，代码更加简洁    
js标准，jquery第三方公司(民间标准)，风光10年，完全替代js    
新型anglarjs、react、vue，js框架，更厉害。vue基于js和jQuery  
在国外react，在国内vue，阿里收购 vue3.0，主流vue2.0  
jquery过渡，它是vue底层技术之一  

```
document，$()  
document.getElementsByTagName("a")，$("a")  
document.getElementsByClassName("username")，$(".username")  
document.getElementsByName("username")，  
	<input type="text" name="username"/>   $("input[type='text']")  
																	$(":text")  
document.getElementById("username"),			$("#username")  

input.value		$("#username").val()   
div.innerText		$("#msg").text()
```

### 小结：jQuery特点
1）简化js语法，非常简洁    
2）选择器：$(selector).action()		  
selector称为选择器：.username(class)，#username(id)  
action()动作（函数）  

使用jQuery需要额外导入js文件  
```<script src="jquery.js"></script>  ```

### json 
交换数据：两台计算机通讯，计算机+网络 TCP/IP，不能传递java对象  
txt（1000,2000,3000）没有层次结构  
xml（<price>1000</price>）自定义标签，比txt强，知道是什么数据，标签量非常大，远超数据  
造成网络传输数据量增加，性能低  
json（也有标签，[]数组、{}一条记录、"key","value"，属性和值）。它既能表示字段是谁，有能有数据  

放京东商城，获取某个商品的价格（链接来自京东公开地址（说明书），爬虫，自己分析网站页面  ）  
https://p.3.cn/prices/mgets?skuIds=J_100008348542  

JSON字符串，返回是一个数组（[]）数组中只有一条记录{}"  
"p"代表商品价格，"id"代表商品编号：100008348542  
```
[
	{
		"p":"4879.00",
		"op":"5299.00",
		"cbf":"0",
		"id":"J_100008348542",
		"m":"6000.00"
	}
]
```

###ajax
提交请求：https://p.3.cn/prices/mgets?skuIds=J_100005185603  
请求京东网站，通过ajax技术发出请求（程序，相当于浏览器发起请求），京东服务器进行响应  
在内部进行查询，查询这个商品信息，把信息转换json字符串返回给请求（响应）  
ajax请求的返回值最终就是json字符串  

### jquery.ajax() 常用有7个参数
type: 	GET/POST，链接形式差异    
url: 	京东链接  
data: 	参数对象  

contentType：	发送请求格式：application/json;charset=utf-8;  
dataType: 	返回数据格式：json/jsonp 跨域（后期京淘项目）  

success: 	访问成功（data），data就是返回json对象  
error(e)：	访问出错，网站返回错误信息  

### 小结
1）jQuery 封装javascript，语法更加简洁，$()替代document，形成新api  
	api死记，$("selector").action()  选择器，执行方法和js不同  
	学习成本很高，需要额外记忆  
	Vue 没有几个api，数据驱动  
2）JSON，本质字符串，远程数据传输，数据交换格式  
	非常简单：[数组]，{一条记录}，key:value。属性：值  
	比txt有结构，比xml没有标签额外占用网络资源，内存资源  
```	
[
	{
		"p":"4879.00",
		"op":"5299.00",
		"cbf":"0",
		"id":"J_100008348542",
		"m":"6000.00"
	}
]
```

	JSON提供方法，把json字符串转换js对象    
	var json = JSON.parse(jsonstr);  
	json[0].id 获取js对象编号，json[0].p 获取js对象的价格  
3）ajax  
	7个常见的参数  
	form表单一般网站提交submit，几乎没人使用；阻塞（同步）  
	ajax异步，多线程并发  




