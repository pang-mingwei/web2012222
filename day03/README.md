# 知识回顾
### 职业规划
1）前端：前端开发工程师（10），走不远，30岁就被减薪，7k
2）后端：java开发工程师，走很远

（70）java初级工程师，中级工程师，

（前端很难）
高级工程师，系统分析师
架构师，技术总监，CTO

互联网架构，大数据，人工智能

管理岗位：小组长，项目经理，部门经理（java开发二部），技术总监，CTO

教学：讲师、A级讲师，金牌讲师，研发总监，教学总监

### html升级h5，css升级css3
引入新的标签，css3引入动画特效，视频音频

transition 伸缩，animation 动画
animation run 4s linear infinite
run定义动画名称 @keyframes关键帧from、to  
4s 动画执行耗时  
linear 动画执行时匀速执行
infinite 反复播放

## 今日任务
1）form表单
2）boostrap 它是css3典型应用，升级
3）升级动态网站 javascript 语法，脚本script语言（sql脚本语言，数据库场合）浏览器场合，弱语言

### html提供表单
表单的页面的内容就可以进行输入  
表单是活的，有填写用户可以决定填写内容  

### bootstrap过渡，最终转向element-ui（支持vue）
bootstrap样式美化，封装css3，形成一套漂亮界面ui（user interface）  
twitter出品，使用bootstrap3  

css是浏览器直接支持，直接写代码就生效  
bootstrap是第三方，预定义class，  
1）html页面引入css样式表文件  
在head标签中增加link标签，两个属性rel，href    
https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css  

2）在标签上就可以加入class="form-group"

### JavaScript
JavaScript和Java有关系吗？没关系，侧热度  
Java 强语言，几乎什么都能干，C/S它能写，B/S它能写（JSP）  
JavaScript 网景和sun合作，改成JavaScript 只用于浏览器，弱语言  
体现：删除本机文件，java直接执行，javascript不允许！（大家都可以浏览，不能别人的文件）
  
开发语言鄙视链：  
c鄙视java，java鄙视c#，c#鄙视javascript，javascript鄙视sql

B/S盛行，browser浏览器，javascript升级ES5/ES6（主流）/ES7，typescript向强语言发展

### javascript作用
访问网页元素，可以和后台程序对接，实现前后台贯穿  
传输后台的信息不包括美化css，label不包括，表单元素  
怎么拿到元素内容？姓名input框的内容？  
体系：DOM（页面元素获取，编辑）、BOM（浏览器控制）
DOM document object model 文档对象模型
BOM browser object model 浏览器对象模型

javascript会把html页面整个抽象，去除杂质，页面元素（div/input/button...）
形成tree树，DOM树，
为什么要抽象出DOM树？为了页面元素快速定位，获取它信息
网页html解析，从上到下，从左到右，页面button，76行，按顺序解析性能低；
tree树
document就是树根，它代表整个html页面
页面上的所有元素就被抽象成tree树上的节点node
树形结构一旦形成，它检索数据速度远高于html顺序解析

![](README_files/1.jpg)

##总结：
1）form表单 <form></form> 提交到后台java实现
	<input>标签：text文本框、number数字框、radio单选框、checkbox复选框、
	<select>选择框，button按钮	<button>
2）美化自己写css，工作量太大，使用第三方boostrap
怎么引入第三方css样式表？
a. 用link标签引入
		<link rel="stylesheet" href="css/bootstrap.min.css" />
b. 可以直接使用这些样式 form-group、form-control
	看官网，看网上手册：bootstrap

3）网页三剑客：h5、css3、javascript（js）
h5 页面展现  
css3 页面美化，boostrap底层就是css语法  
h5+css3实现静态网站，网站内容是不能修改的  
javascript 实现网站交互，js修改页面元素，提交后端去处理java（ajax+json)
js引入之后，从静态网站扩展到动态网站
h5+css3+js+ajax+json+ssm三大框架+mysql 整个业务系统开发基本技术

javascript升级jQuery（过渡），jQuery升级Vue

4）var变量、let变量、const常量、object对象、function函数、arrow function箭头函数
5）object初始化写法、动态写法





