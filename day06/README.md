## 知识回顾
### Vue前端框架
javascript和jQuery与框架Vue有什么不同呢？
关注点，js关注基础实现，最早诞生，关注浏览器如何增强交互功能。
jQuery关注简化，优化，少写多做！本质是在于js之上诞生，小的格局变化，并没有根本性改变。
Anglur开了框架的先河，分层概念TCP/IP分层7层协议（网络，硬件+软件），j2ee框架分层概念，MVC架构，三大框架，微服务
大前端，reactjs和vuejs。
框架：它做好了一些基本事情，这些事情无需开发人员写代码，人家直接实现。
写代码都是在这些框架基础写，
关注点：框架思想应用，渐进式、数据驱动（data，插值表达式，标签的属性）、组件化（代码复用）

### Vue.js 渐进式
1）vue.js
2）vue.js、组件化、npm管理依赖（js库）、webpack打包工具

### 数据驱动
框架，死板，写代码约束。文件名起什么名字，放在什么目录中都有规定。components/Item.vue，App.vue引入组件
按规定来写代码，它就自动连入到整体项目中，不用开发者额外写代码（术语：约定大于配置）
数据驱动带来了新的革命，开发者关注点（学习要点）由js的api，变成关注结构，代码该在哪里去写，语法结构是什么的

MVVM框架  
写数据就放模型Model的位置，写页面展示放视图View的位置，剩下事情VM去做（vue它连接模型和视图，渲染）。  

### Vue要实现这种机制，提供很多新的方式
Vue对象结构，el挂载点，data数据区，methods函数区，还有很多区域。
注意数据区数据写法，函数区函数写法，和js不同，不能混用。

### Vue实现数据展示
双向绑定：js/jquery单向的，它们操作dom树，更新页面。Vue是双向，页面变化数据也变化，数据变化页面引用变化。  
1）插值表达式：{{msg}}，Vue底层会遍历这些插值表达式，去数据区找对应变量，用变量值替换  
2）v-指令集：Vue编译作用，把对应指令翻译html语句，v-show,v-if,v-for,....  
通过上面两种方式，动态展现页面元素。

##今日内容，Vue项目（工程）10遍（练习少，胸中没有竹子，经验积累）
1）添加组件，商品管理（图书：title标题、sellpoint卖点、price价格、说明intro），如何复用  
2）商品管理：新增、修改、删除、列表 CRUD，包括之间关系，包括出现问题，问题怎么解决的  
3）element-ui从官网一步一步写出来  

### 写自己的组件
1）定义组件：src/components/Item.vue  
2）注册组件：App.vue根组件，我们定义组件都是根组件App的子组件，引入Item.vue组件  
3）使用组件：<Item></Item>，Vue就会翻译成<template>标签的内容+data结合  

引入element-ui，有一系列组件ui：布局、表单、表格、对话框、输入框、文本域  
里面有业务逻辑，新增、修改、删除、列表，它们怎么有机联系起来  
数据，对象数组模拟  

### 安装 element-ui
npm 管理第三方插件（组件）  
安装命令：  
npm i element-ui -S  
参数：i安装install，插件名称，-S安装生产环境，-D开发环境，测试环境  

### 把element-ui 引入到jt项目中
修改main.js，全局js和css引入地方，加3句话：   

组件导入方式，全局，各个组件可以调用这个组件  
```
import ElementUI from 'element-ui';  
import 'element-ui/lib/theme-chalk/index.css';  

Vue.use(ElementUI);  
```
### 栅格设计
为了适合PC屏幕和手机屏幕，把屏幕24列  
```
<el-row> 
	<el-col>
</el-row>
```

### 数组新增、修改、删除元素
三个参数：  
1）参数1：索引位置  
2）参数2：删除个数  
3）参数3：新增的值  

list.splice(0,0,"chen");		新增  
list.splice(2,1,"tony");		修改  
list.splice(2,1);				删除  

### 删除
每行遍历时是有一个索引值index，Vue插槽slot-scope，提供变量s，它里面两个值  
s.$index 获取当前行的索引值  
s.row，代表当前行数据（list[2]）  

list.splice(index,1) 数组中把当前行删除  

### 修改
1）得到当前行的索引，row=list[index]，获取到当前行数据，把它m数据去变量 this.m=row;  
2）打开对话框，把值“回显”对话框中的input框中，表单form，关联 :model="m"  

3）点击确定按钮，save方法，加参数具体值m，修改页面值，它通过双向绑定，就改变数据区m中属性  
保存这个值，splic函数  
4）设置一个变量，isUpdate，是否修改 新增：isUpdate=false；修改：isUpdate=true  
新增和修改是公用save方法，通过这个参数调用不同splice，if判断  

### 过程中还有问题：副作用，消除
解决办法：巧妙实现，利用js提供JSON方法实现！  
JSON js子集，浏览器直接支持语法  
JSON.parse()		//把json字符串转换js对象  
JSON.stringify()  //把js对象转换json字符串  

this.m = row	 js对象  
this.m = JSON.parse(JSON.stringify(row));  //经过2次转换，创建新对象 new Object()  
不同的引用，修改form表单框中内容，this.m，这时和页面row就没有关系  

### SPA
现在主流方式单页面SPA，Vue框架推崇单页面，不用刷新  




